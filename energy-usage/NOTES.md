Candidate Name: Dan Belsey

Tasks: 2 + 3

Time: 2 hours Pt 2, 1.5 hours Pt 3

Notes:

## General

I had a lot of issues getting Koa / Koa Router to work; following various examples (including from the official docs) didn't seem to work right. In particular, I couldn't get the status to change by calling `ctx.status = 200` under any circumstances, even though the docs suggest that should work. I found a comment online that said that making handlers `async` or return Promises worked - I opted for the latter, as I found it slightly easier to work with given sqlite's callback-based interface.

I had similar issues with the API tests; I googled around and most people recommended supertest, but when using it I couldn't get the API handlers to read the request parameters; either the supertest examples I based it on don't work the way I expect, or the Koa docs for reading requests don't.

As a result the API layer itself is really poor - sorry! I spent a good hour or so just trying to make it work, unsuccessfully, before deciding to focus my time on the queries and calculations instead. If I had more time I'd read more into the Koa/router docs, make that layer work, and then begin on validating payloads better and throwing better errors.

Rather than learn your lint config I also added a command to auto-fix linting problems - your style's a little different to what I'm used to, so wanted to make sure it was right! (I'm curious why `no-plusplus` is a rule!)

## Testing

I set up some basic testing tear down functions in `data.js` - they're not particularly elegant, but were sufficient for setting up a clean testing slate for each block of tests. In reality I'd probably refine this down and insert more relevant data for each test, but it seemed unnecessary at this stage.

## Task 2

Aside from the API issues above, the handler for getting readings simply maps to a database call. I chose to sort it in order with the most recent first (as I'd expect users are more likely to be interested in their recent data), but it's easy to change to other orders if required.

The handler for posting a new reading takes in a date and reading (but no unit, as requirements said it was always kWh) and insert it into the database. I did some incredibly basic payload validation (I'm unsure if Koa Router has any built-ins to help with this), but with more time and clearer requirements there are lots of potential additions to validation - e.g.:
- checking that the date is not in the future
- checking the date is not before the previous reading
- checking reading is non-negative, and greater than previous reading


## Task 3

Given previous API problems I decided to extract the calculations into a separate file, `estimates.js` and test the algorithm independently of the API; I've sketched out what the API handler would look like but haven't tested that layer.

Both algorithms perform some checks on the length of data provided, as (if I've understood correctly) both stages need more than one data point to produce any results.

`estimateEndOfMonthReadings` works using the provided `util` functions to calculate:
1) The readings and dates of the readings either side of the last day of the month
   1.1) If one of the readings is already the end of the month then I've used that value literally, rather than estimating it
2) Working out the number of days between readings, and total energy used between readings
3) Working out energy / day, then multiplying by number of days remaining, to get the expected energy use for the "rest" of the month
4) Adding it to the previous reading and returning it.

The requirements didn't specify a format, so I chose a string that specifies month and year in a human-friendly format; in hindsight this may not have been the best choice at this stage as it makes it more difficult to sort, and I'd probably go back and change it to e.g. `01-2018`.

`calculateUsage` simply works out the difference between end-of-month estimates calculated above and returns them in a similar format, e.g. `{month: 'Jan 2018', usage: 1234}`. If the API handler worked properly, this is what it'd return!
