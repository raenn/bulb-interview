const { expect } = require('chai');
const moment = require('moment');

const data = require('./data');
const sampleData = require('../sampleData.json');


describe('data', () => {
  before(() => data.reset());

  it('initialize should import the data from the sampleData file', (done) => {
    data.initialize();

    data.connection.serialize(() => {
      data.connection.all('SELECT * FROM meter_reads ORDER BY cumulative', (error, selectResult) => {
        expect(error).to.be.null;
        expect(selectResult).to.have.length(sampleData.electricity.length);
        selectResult.forEach((row, index) => {
          expect(row.cumulative).to.equal(sampleData.electricity[index].cumulative);
        });
        done();
      });
    });
  });

  describe('.getReadings', () => {
    before(() => data.resetToSample());

    it('should get all readings, ordered by date', (done) => {
      data.getReadings((error, readings) => {
        expect(error).to.be.null;
        expect(readings).to.have.length(sampleData.electricity.length);
        // results are in reverse order to order of sample data
        readings.forEach((row, index) => {
          if (index === 0) {
            const mostRecent = sampleData.electricity[sampleData.electricity.length - 1];
            expect(row.reading_date).to.equal(mostRecent.readingDate);
            return;
          }
          // for all readings but the first, check the date order is correct
          const previousReading = readings[index - 1];
          expect(moment(row.reading_date).isBefore(previousReading)).to.equal(true);
        });

        done();
      });
    });
  });

  describe('.insertReading', () => {
    before(() => data.resetToSample());
    /* possible future tests:
     * check current cumulative >= most recent cumulative
        > I looked it up and it might be possible to go backwards if you have your own power supply!
     * check reading is after last reading (may not be desirable? depends who uses this API)
     * check reading is not in the future
     */

    it('should insert a reading in kWH', (done) => {
      const cumulative = 12345;
      const readingDate = '2018-09-24T21:46:26.000Z';
      const expectedUnit = 'kWh';

      data.insertReading(cumulative, readingDate, (error) => {
        expect(error).to.be.null;

        data.connection.serialize(() => {
          data.connection.get(
            'SELECT * FROM meter_reads WHERE reading_date = ?',
            readingDate,
            (error2, row) => {
              expect(error2).to.be.null;
              expect(row.cumulative).to.equal(cumulative);
              expect(row.reading_date).to.equal(readingDate);
              expect(row.unit).to.equal(expectedUnit);
              done();
            },
          );
        });
      });
    });
  });
});
