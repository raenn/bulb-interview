const utils = require('./utils');
const moment = require('moment');

const estimateEndOfMonthReadings = (readings) => {
  if (readings.length === 0) { return []; }

  const estimates = [];
  const addEstimate = (date, estimate) => {
    estimates.push({
      month: moment(date).format('MMM YYYY'),
      estimate,
    });
  };

  for (let i = 0; i < readings.length - 1; i += 1) {
    const thisReadingMmt = moment(readings[i].reading_date);
    const nextReadingMmt = moment(readings[i + 1].reading_date);
    // if we have the actual value, no need to estimate
    if (utils.isEndOfMonth(thisReadingMmt)) {
      addEstimate(readings[i].reading_date, readings[i].cumulative);
    } else {
      const energyUsed = readings[i + 1].cumulative - readings[i].cumulative;
      const days = utils.getDiffInDays(nextReadingMmt, thisReadingMmt);
      const energyPerDay = energyUsed / days;

      const daysRemaining = utils.getDaysUntilMonthEnd(thisReadingMmt);
      const predictedRemaining = energyPerDay * daysRemaining;

      const endOfThisMonthMmt = thisReadingMmt.endOf('month');
      addEstimate(endOfThisMonthMmt, readings[i].cumulative + predictedRemaining);
    }
  }

  return estimates;
};

const calculateUsage = (estimates) => {
  if (estimates.length === 0) { return []; }
  const monthlyUsages = [];
  for (let i = 1; i < estimates.length; i += 1) {
    monthlyUsages.push({
      month: estimates[i].month,
      usage: estimates[i].estimate - estimates[i - 1].estimate,
    });
  }

  return monthlyUsages;
};

module.exports = {
  estimateEndOfMonthReadings,
  calculateUsage,
};
