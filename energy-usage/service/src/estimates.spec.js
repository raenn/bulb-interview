const { expect } = require('chai');
const estimates = require('./estimates');

const { calculateUsage, estimateEndOfMonthReadings } = estimates;

describe('estimateEndOfMonthReadings', () => {
  it('should return empty array if given empty array', (done) => {
    expect(estimateEndOfMonthReadings([])).to.be.an('array').that.is.empty;
    done();
  });

  it('should return empty array if given one reading', (done) => {
    const readings = [
      {
        cumulative: 17580,
        reading_date: '2017-03-28T00:00:00.000Z',
        unit: 'kWh',
      },
    ];
    expect(estimateEndOfMonthReadings(readings)).to.be.an('array').that.is.empty;
    done();
  });

  it('should return one estimate if given two readings', (done) => {
    const readings = [
      {
        cumulative: 17580,
        reading_date: '2017-03-28T00:00:00.000Z',
        unit: 'kWh',
      },
      {
        cumulative: 17759,
        reading_date: '2017-04-15T00:00:00.000Z',
        unit: 'kWh',
      },
    ];
    const expectedEstimate = readings[0].cumulative + 29.83333;
    const expected = [
      { month: 'Mar 2017', estimate: expectedEstimate },
    ];
    const actual = estimateEndOfMonthReadings(readings);
    expect(actual.length).to.equal(1);
    expect(actual[0].month).to.equal(expected[0].month);
    expect(actual[0].estimate).to.be.approximately(expected[0].estimate, 0.01);
    done();
  });
});

describe('calculateUsage', () => {
  it('should return empty array if given empty array', (done) => {
    expect(calculateUsage([])).to.be.an('array').that.is.empty;
    done();
  });

  it('should return empty array if given single estimate', (done) => {
    const estimate = { month: 'Jan 2018', estimate: 100 };
    expect(calculateUsage([estimate])).to.be.an('array').that.is.empty;
    done();
  });

  it('should return usage for one month if given two estimates', (done) => {
    const fakeEstimates = [
      { month: 'Jan 2018', estimate: 100 },
      { month: 'Feb 2018', estimate: 234 },
    ];
    const actual = calculateUsage(fakeEstimates);
    expect(actual.length).to.equal(1);
    expect(actual[0].month).to.equal(fakeEstimates[1].month);
    expect(actual[0].usage).to.equal(134);
    done();
  });
});
