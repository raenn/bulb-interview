const Koa = require('koa');
const KoaRouter = require('koa-router');
const data = require('./data');
const bodyParser = require('koa-bodyparser');
const estimates = require('./estimates');
const moment = require('moment');

const PORT = process.env.PORT || 3000;

function createServer() {
  const server = new Koa();

  const router = new KoaRouter();
  router.get('/', (ctx, next) => {
    ctx.body = 'Hello world';
    next();
  });

  router.get('/meter/readings', ctx => new Promise((resolve) => {
    data.getReadings((err, readings) => {
      ctx.body = readings;
      resolve();
    });
  }));

  router.post('/meter/reading', ctx => new Promise((resolve) => {
    const { date, reading } = ctx.request.body;
    if (!date) { ctx.throw(400, 'date is required'); }
    if (!reading) { ctx.throw(400, 'reading is required'); }

    data.insertReading(date, reading, () => {
      ctx.status = 201;
      resolve();
    });
  }));

  router.get('/usage/monthly', ctx => new Promise((resolve) => {
    data.getReadings((err, readings) => {
      // actually needed readings in original order in the end! whoops.
      const sortedReadings = readings.sort((a, b) =>
        moment(a.reading_date).diff(moment(b.reading_date)));
      const estimatedReadings = estimates.estimateEndOfMonthReadings(sortedReadings);
      ctx.body = estimates.calculateUsage(estimatedReadings);
      resolve();
    });
  }));

  server.use(bodyParser());
  server.use(router.allowedMethods());
  server.use(router.routes());

  return server;
}

module.exports = createServer;

if (!module.parent) {
  data.initialize();
  const server = createServer();
  server.listen(PORT, () => {
    console.log(`server listening on port ${PORT}`);
  });
}
