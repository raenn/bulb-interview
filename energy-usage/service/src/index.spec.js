const Koa = require('koa');
const { expect } = require('chai');

const server = require('./index');
const sampleData = require('../sampleData.json');
const data = require('./data');

const chai = require('chai');

const should = chai.should();
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

before(() => data.initialize());
afterEach(() => data.resetToSample());

describe('index', () => {
  it('should create an instance of a Koa server', () => {
    const instance = server();
    expect(instance).to.be.instanceof(Koa);
  });
});

describe('GET /usage/monthly', () => {
  it('should get monthly usage', (done) => {
    const instance = server();

    chai.request(instance.callback())
      .get('/usage/monthly')
      .end((err, response) => {
        should.not.exist(err);
        response.status.should.eql(200);
        response.type.should.eql('application/json');

        response.body.length.should.eql(12);
        done();
      });
  });
});

describe('GET /meter/readings', () => {
  it('should get all readings, with most recent first', (done) => {
    const instance = server();

    chai.request(instance.callback())
      .get('/meter/readings')
      .end((err, response) => {
        should.not.exist(err);
        response.status.should.eql(200);
        response.type.should.eql('application/json');
        const expected = sampleData.electricity;
        expect(response.body.length).to.equal(expected.length);

        // data is in reverse order
        expect(response.body[0].cumulative).to.equal(expected[expected.length - 1].cumulative);
        expect(response.body[1].cumulative).to.equal(expected[expected.length - 2].cumulative);
        expect(response.body[response.body.length - 1].cumulative)
          .to.equal(expected[0].cumulative);
        done();
      });
  });
});

describe('POST /meter/reading', () => {
  it('should fail if `cumulative` is missing', (done) => {
    const instance = server();

    chai.request(instance.callback())
      .post('/meter/reading')
      .send({ date: '2017-03-28T00:00:00.000Z' })
      .end((err, res) => {
        should.exist(err);
        res.status.should.equal(400);
        err.response.text.should.equal('reading is required');
        done();
      });
  });

  it('should fail if `date` is missing', (done) => {
    const instance = server();

    chai.request(instance.callback())
      .post('/meter/reading')
      .send({ reading: 12345 })
      .end((err, res) => {
        should.exist(err);
        res.status.should.equal(400);
        err.response.text.should.equal('date is required');
        done();
      });
  });

  it('should store a valid reading in kWh', (done) => {
    const instance = server();

    chai.request(instance.callback())
      .post('/meter/reading')
      .send({ reading: 12345, date: '2017-03-28T00:00:00.000Z' })
      .end((err, res) => {
        should.not.exist(err);
        res.status.should.eql(201);
        done();
      });
  });
});
